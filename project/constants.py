import uuid
from pathlib import Path


LINK = 'http://automationpractice.com/index.php'
CHROME_PATH = Path(__file__).parent.joinpath('chromedriver')
EMAIL = f'{str(uuid.uuid1())}@test.com'

