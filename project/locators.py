from selenium.webdriver.common.by import By


class MainContainers:
    banner = (By.CLASS_NAME, 'banner')
    navbar = (By.CLASS_NAME, 'nav')
    logo = (By.ID, 'header_logo')
    menu = (By.ID, 'block_top_menu')
    center_column = (By.XPATH, "//div[@id='center_column']")
    product_content = (By.XPATH, "//div[@class='tab-content']")
    popularity_selection = (By.XPATH, "//ul[@id='home-page-tabs']")
    lower_content_container = (By.XPATH, "//div[@id='htmlcontent_home']")
    facebook_column = (By.XPATH, "//div[@id='facebook_block']")
    contact_column = (By.XPATH, "//div[@id='cmsinfo_block']")
    footer = (By.XPATH, "//div[@class='footer-container']")


class Main:
    breadcrumb = (By.XPATH, '//div[class ="breadcrumb clearfix"]')
    btn_signIn = (By.XPATH, '//a[@class="login"]')
    inp_search = (By.XPATH, '//input[@id="search_query_top"]')
    btn_search = (By.XPATH, '//button[@name="submit_search"]')
    btn_cart = (By.XPATH, '//a[@title="View my shopping cart"]')
    btn_contactUs = (By.XPATH, '//a[@title="Contact Us"]')
    btn_Women = (By.XPATH, '//li//a[@title="Women"]')
    dd_women_bloses = (By.XPATH, '//a[@title="Blouses"]')
    btn_Dresses = (By.XPATH, '//li//a[@title="Dresses"]')  # second
    btn_tShirt = (By.XPATH, '//li//a[@title="T-shirts"]')  # second
    section_popular = ('//a[@href="#homefeatured"]')
    section_bestSellers = ('//a[@href="#blockbestsellers"]')
    add_to_cart = ('//span[text()="Add to cart"]')
    btn_more = ('//span[text()="More"]')
    product_img = ('//a[@class="product_img_link"]//img')
    product_price = ('//div[@itemprop="offers"]//span[@itemprop="price"]')  # every second
    btn_user = (By.XPATH, '//a[@title="View my customer account"]')  # every second


class Authentication:
    inp_newEmail = (By.XPATH, '//input[@id="email_create"]')
    btn_createAccount = (By.XPATH, '//button[@id="SubmitCreate"]')
    inp_existingEmail = (By.XPATH, '//input[@id="email"]')
    inp_password = (By.XPATH, '//input[@id="passwd"]')


class CreateAccount:
    cbx_gender2 = (By.ID, 'id_gender2')
    customer_firstname = (By.ID, 'customer_firstname')
    customer_lastname = (By.ID, 'customer_lastname')
    passwd = (By.ID, 'passwd')
    days = (By.ID, 'days')
    days_opt = (By.XPATH, "//select[@id='days']/option[2]")
    months = (By.ID, 'months')
    months_opt = (By.XPATH, "//select[@id='months']/option[4]")
    years = (By.XPATH, "//select[@id='years']")
    years_opt = (By.XPATH, "//select[@id='years']/option[20]")

    newsletter = (By.XPATH, "//input[@id='newsletter']")
    offers = (By.XPATH, "//input[@id='optin']")
    firstname = (By.XPATH, "//input[@id='firstname']")
    lastname = (By.XPATH, "//input[@id='lastname']")
    company = (By.XPATH, "//input[@id='company']")
    address1 = (By.XPATH, "//input[@id='address1']")
    address2 = (By.XPATH, "//input[@id='address2']")
    city = (By.XPATH, "//input[@id='city']")
    id_state_opt = (By.XPATH, "//select[@id='id_state']/option[2]")

    id_state = (By.ID, 'id_state')
    postcode = (By.ID, 'postcode')
    additional_text = (By.ID, 'other')
    id_country = (By.ID, 'id_country')
    phone_home = (By.ID, 'phone')
    phone_mobile = (By.ID, 'phone_mobile')
    alias = (By.ID, 'alias')
    register = (By.NAME, 'submitAccount')


class Blouses:
    items = (By.XPATH, '//ul[@class="product_list grid row"]')
    size_s = (By.XPATH, '//input[@name="layered_id_attribute_group_1"]')
    item_info = (By.XPATH, '//div[@class="right-block"]')
    add_to_cart = (By.XPATH, '//span[text()="Add to cart"]')
    btn_checkout = (By.XPATH, '//a[@title="Proceed to checkout"]')


class Order:
    btn_checkout = (By.XPATH, '//span[text()="Proceed to checkout"]')


class Shipping:
    btn_checkout = (By.NAME, 'processCarrier')
    cbx_agreements = (By.ID, 'cgv')


class Address:
    btn_checkout = (By.XPATH, '//span[text()="Proceed to checkout"]')


class Payment:
    btn_byBank = (By.CLASS_NAME, 'bankwire')
    btn_confirm = (By.XPATH, "//span[contains(text(),'I confirm my order')]")


class MyAccount:
    btn_orderHistory = (By.XPATH, '//span[text()="Order history and details"]')
    table_lines = (By.TAG_NAME, 'tr')
