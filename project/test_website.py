from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC

from .locators import *
from .constants import LINK, CHROME_PATH, EMAIL


def wait_page_loaded():
    page_loaded = False
    counter, timeout, first_sleep_time = 0, 10, 1
    sleep(first_sleep_time)

    # Wait until page loaded
    while not page_loaded:
        sleep(1)
        counter += 1
        try:
            # Scroll, to make sure all objects will be loaded
            driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')
            page_loaded = driver.execute_script("return document.readyState == 'complete';")
        except Exception as e:
            pass

        assert counter < timeout, f'The page loaded more than {timeout+first_sleep_time} seconds!'

    driver.execute_script('window.scrollTo(document.body.scrollHeight, 0);')


def wait_url_changes(url):
    WebDriverWait(driver, 15).until(EC.url_changes(url))


def wait_for_element_and_click(target):
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(target))
    element(*target).click()


def send_text(target, text):
    ActionChains(driver).double_click(element(*target)).send_keys(Keys.DELETE).send_keys(text).perform()


driver = webdriver.Chrome(CHROME_PATH)
element = driver.find_element
elements = driver.find_elements


def test_page_is_loaded():
    driver.get(LINK)
    wait_page_loaded()
    elements_to_check = {k:v for k,v in MainContainers.__dict__.items() if not k.startswith('__')}
    for name, selector in elements_to_check.items():
        assert element(*selector), f'element is not found {name} {selector}'


def test_user_registration():
    current_url = driver.current_url
    element(*Main.btn_signIn).click()
    wait_url_changes(current_url)

    element(*Authentication.inp_newEmail).send_keys(EMAIL)

    current_url = driver.current_url
    element(*Authentication.btn_createAccount).click()
    wait_url_changes(current_url)

    element(*CreateAccount.cbx_gender2).click()
    send_text(CreateAccount.customer_firstname, 'Samantha')
    send_text(CreateAccount.customer_lastname, 'Smith')
    send_text(CreateAccount.passwd, 'qwerty')

    element(*CreateAccount.days).click()
    element(*CreateAccount.days_opt).click()

    element(*CreateAccount.months).click()
    element(*CreateAccount.months_opt).click()

    element(*CreateAccount.years).click()
    element(*CreateAccount.years_opt).click()

    element(*CreateAccount.newsletter).click()
    element(*CreateAccount.offers).click()

    send_text(CreateAccount.firstname, 'Samantha')
    send_text(CreateAccount.lastname, 'Smith')

    send_text(CreateAccount.company, 'Company')
    send_text(CreateAccount.address1, 'Test Street 1')
    send_text(CreateAccount.address2, 'Test Street 2')
    send_text(CreateAccount.city, 'City')

    element(*CreateAccount.id_state).click()
    element(*CreateAccount.id_state_opt).click()

    send_text(CreateAccount.postcode, '00000')
    send_text(CreateAccount.additional_text, 'this is test text')
    send_text(CreateAccount.phone_home, '01234')
    send_text(CreateAccount.phone_mobile, '01234')
    send_text(CreateAccount.alias, 'alias')

    current_url = driver.current_url
    element(*CreateAccount.register).click()
    wait_url_changes(current_url)


def test_select_product():
    element(*MainContainers.logo).click()
    Hover = ActionChains(driver).move_to_element(element(*Main.btn_Women))\
        .move_to_element(element(*Main.dd_women_bloses))
    Hover.click().perform()

    assert element(*Blouses.items).find_element_by_tag_name('li').\
               find_element_by_tag_name('div').get_attribute('class') == 'product-container'

    Hover = ActionChains(driver).move_to_element(element(*Blouses.item_info))\
        .move_to_element(element(*Blouses.add_to_cart))
    Hover.click().perform()


def test_order_product():
    wait_for_element_and_click(Blouses.btn_checkout)
    wait_for_element_and_click(Order.btn_checkout)
    wait_for_element_and_click(Address.btn_checkout)
    element(*Shipping.cbx_agreements).click()
    element(*Shipping.btn_checkout).click()
    wait_for_element_and_click(Payment.btn_byBank)
    element(*Payment.btn_confirm).click()
    assert 'Your order on My Store is complete.' in driver.page_source


def test_order_history():
    element(*Main.btn_user).click()
    element(*MyAccount.btn_orderHistory).click()
    assert len(list(filter(lambda x: x.get_attribute('class'), elements(*MyAccount.table_lines)))) == 1
    driver.close()
