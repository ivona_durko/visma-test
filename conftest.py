import pytest


@pytest.fixture(scope="session", autouse=True)
def init():
    print('\nINITIALIZATION\n')
    yield
    print('\nTEAR DOWN\n')